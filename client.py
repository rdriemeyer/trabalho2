import requests, json

body = {
    "message": "hello world"
}

body = json.dumps(body)

headers = {'Content-Type': 'application/json'}

r = requests.post("http://localhost/upper", data=body, headers=headers)

j = r.json()
print(j['message'])